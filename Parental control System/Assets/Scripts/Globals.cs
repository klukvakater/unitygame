﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class Globals : MonoBehaviour {
    public  static int time;
	// Use this for initialization
	void Start () {
        if (time != 0)
        {
            StartCoroutine(Wait(time));
        }
	}
    private IEnumerator Wait(int time)
    {
        yield return new WaitForSeconds(time); // таймер, через 10 секунд
        SceneManager.LoadScene(0); // выполнится эта строка
    }

    // Update is called once per frame
    void Update () {
		
	}
}
