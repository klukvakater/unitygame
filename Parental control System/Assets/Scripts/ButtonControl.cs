﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class ButtonControl : MonoBehaviour {
    MenuControl menuControl;
     public InputField inputData;

    void Start()
    {
        menuControl = GameObject.FindGameObjectWithTag("GameManager").GetComponent<MenuControl>();
    }

    public void LoadCanvas(string canvasIndex)
    {
        menuControl.canvasIndex = canvasIndex;
    }


    public void setting()
    {
        Globals.time = Convert.ToInt32(inputData.text);
        Globals.time = Globals.time * 60;
       // StartCoroutine(Wait(Globals.time));
    }
    public void LoadLevel1()
    {
        SceneManager.LoadScene(1);
    }

    public void inputValues()
    {
        
    }

    private IEnumerator Wait(int time)
    {
        yield return new WaitForSeconds(time); // таймер, через 10 секунд
        SceneManager.LoadScene(0); // выполнится эта строка
    }
}
